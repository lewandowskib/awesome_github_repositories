# Awesome github repositories



### Collection of links

[The book of secret knowledge][cookbook]
A collection of inspiring lists, manuals, cheatsheets, blogs, hacks, one-liners, cli/web tools and more.

[build-your-own-x][build-your-own-x] Build your own (insert technology here)

[awesome-sysadmin][awesome-sysadmin] A curated list of amazingly awesome open source sysadmin resources inspired by Awesome PHP.
<!-- blank line -->

----
[awesome-sysadmin]: https://github.com/kahun/awesome-sysadmin#awesome-sysadmin
[build-your-own-x]: https://github.com/danistefanovic/build-your-own-x#build-your-own-game
[cookbook]: https://github.com/trimstray/the-book-of-secret-knowledge
### Tools
[Keeweb][keeweb] Free cross-platform password manager compatible with KeePass

[HedgeDoc][hedgeDoc] The best platform to write and share markdown.

[Mirotalk][mirotalk] Powered by WebRTC, Real-time video call, chat room and screen sharing entirely browser based

[Compressimage.io][compressimage.io] Compress JPG and PNG images at light speed with Browser based Image compressor

[TinyVid][TinyVid] TinyVideo is built to solve a very modern headache: getting a video to fit a file size limitation.

[Readme.so][readme.so] Our simple editor allows you to quickly add and customize all the sections you need for your project's readme

[Openwrt][openwrt] OpenWrt Project is a Linux operating system targeting embedded devices. Instead of trying to create a single, static firmware, OpenWrt provides a fully writable filesystem with package management. 

[rtty][rtty] Access your terminal from anywhere via the web.

[buster][buster] Buster is a browser extension which helps you to solve difficult captchas by completing reCAPTCHA audio challenges using speech recognition. Challenges are solved by clicking on the extension button at the bottom of the reCAPTCHA widget.

**Docker**

[Yacht][Yacht] A web interface for managing docker containers with an emphasis on templating to provide 1 click deployments.



**SelfHosted**

[Tinyhome][Tinyhome] tinyhome generate an html homepage via shell script.

[Flame][flame] Flame is self-hosted startpage for your server. Easily manage your apps and bookmarks with built-in editors.

[MyDrive][MyDrive] Drive is an Open Source cloud file storage server (Similar To Google Drive). 

[wallabag][wallabag] Wallabag is a self hostable application for saving web pages: Save and classify articles. Read them later. Freely. 

[Home Assistant][Home Assistant]  Open source home automation that puts local control and privacy first

[Navidrome Music Server][Navidrome Music Server] Modern Music Server and Streamer compatible with Subsonic/Airsonic

[Tinystatus][Tinystatus] tinystatus generate an html status page via shell script.

[rtty]: https://github.com/zhaojh329/rtty
[buster]: https://github.com/dessant/buster
[Tinyhome]: https://github.com/Tarow/tinyhome
[Tinystatus]: https://github.com/bderenzo/tinystatus
[Navidrome Music Server]: https://github.com/navidrome/navidrome
[Home Assistant]: https://github.com/home-assistant/core
[wallabag]: https://github.com/wallabag/wallabag
[openwrt]: https://github.com/openwrt/openwrt
[TinyVid]: https://kamua.com/tinyvid/
[MyDrive]: https://github.com/subnub/myDrive
[readme.so]: https://readme.so/
[Yacht]: https://github.com/SelfhostedPro/Yacht
[compressimage.io]: https://compressimage.io/
[flame]: https://github.com/pawelmalak/flame
[mirotalk]: https://github.com/miroslavpejic85/mirotalk
[hedgeDoc]: https://hedgedoc.org/
[keeweb]: https://github.com/keeweb/keeweb
<!-- blank line -->
----
### Games
[Rpg-cli][rpg-cli]  Your filesystem as a dungeon!




[rpg-cli]: https://github.com/facundoolano/rpg-cli
